import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';

// import key from '../services/key';
// import socketMiddleware from './middlewares/socket';
// import logger from './middlewares/logger';
import RootReducer from './reducers';

const middleware = applyMiddleware(
  thunkMiddleware,
  // logger,
  // socketMiddleware(key.serverURL)
);

const Store = createStore(RootReducer, composeWithDevTools(middleware));

export default Store;
