import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';
// ==============================
import { FBkey, GGkey } from './key';
import {
  GoogleOauth,
  FacebookOauth,
  logoutUser
} from '../../../redux/actions/auth.action';
const Login = props => {
  const dispatch = useDispatch();
  const { isAuthenticated } = useSelector(state => state.auth);
  return (
    <div>
      <div className="title">
        <h1>Login page</h1>
        <hr />
      </div>
      <div className="main">
        <FacebookLogin
          appId={FBkey}
          callback={res => dispatch(FacebookOauth(res))}
          render={renderProps => (
            <button
              onClick={() => renderProps.onClick()}
              className="social-signin facebook"
            >
              Log in with Facebook
            </button>
          )}
        />
        <div className="or">or</div>
        <GoogleLogin
          clientId={GGkey}
          render={renderProps => (
            <button
              onClick={() => renderProps.onClick()}
              disabled={renderProps.disabled}
              className="social-signin google"
            >
              Log in with Google+
            </button>
          )}
          buttonText="Login"
          onSuccess={res => dispatch(GoogleOauth(res))}
          onFailure={() => this.responseGoogle()}
          cookiePolicy={'single_host_origin'}
        />
      </div>
      <br></br>
      {isAuthenticated && (
        <>
          <Link to="/">Back to Homepage</Link>
          <div className="or">or</div>
          <button onClick={() => dispatch(logoutUser())}>Logout</button>
        </>
      )}
    </div>
  );
};

Login.propTypes = {};

export default Login;
