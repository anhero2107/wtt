import React from 'react'
//=================================================
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
//=================================================
import PrivateRoute from '../HOCs/PrivateRoute';
import Content from '../components/contents/Content';
import Login from '../components/auth/Login';
//============================================
const Routers = () => {
    return (
        <Router>
        <Route exact path="/" component={Content} />
        <Route exact path="/login" component={Login} />
        <Switch>
          <PrivateRoute exact path="/private" component={Content} />
        </Switch>
      </Router>
    )
}

export default Routers
